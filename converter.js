const fs = require('fs');
const util = require('util');
const encoder = new util.TextEncoder();
let fileBlobs = [];
let gltf;
let remainingFilesToProcess = 0;
let glbFilename;
let outputBuffers; //contains data before it is wrote back to a glb file.
let bufferOffsetMap //contains byte offsets of each output buffer;
let bufferOffset;

const args = process.argv.splice(2)
checkHelp(args)
checkRequirements()

const startTime = getTimeAsMs()
let inputPath = getInputPath(args)
console.error("inputPath :", inputPath)
let outputPath = parseOutputPath(args)

function checkRequirements() {
    if (process.argv.length > 2) {
        console.error("No arguments found. Use -h for help.");
        process.abort();
    }
}

function checkHelp(argv) {
    if (argv.find(value => value === "-h")) {
        console.log("Usage : npm run convert [input_path] [output_path]");
        process.exit(0);
    }
}

function getTimeAsMs() {
    const hrTime = process.hrtime();
    return (hrTime[0] * 1000000 + hrTime[1] / 1000) / 1000;
}

function getInputPath(args) {
    const defaultValue = ".";
    if (args.length === 0) {
        return defaultValue;
    }

    const path = args[0];
    if (path.indexOf(".") !== -1) {
        const slashIndex = path.lastIndexOf("/")

        if (slashIndex === -1) return defaultValue

        return path.substr(0, slashIndex)
    }

    return args[0];
}

function parseOutputPath(args) {
    return args.length === 2 ? args[1] : ".";
}

function processFiles(path, files) {
    remainingFilesToProcess = files.length;
    for (let i = 0; i < files.length; i++) {
        let file = files[i];

        traverseFileTree(path, file);
    }
}

function traverseFileTree(path, file) {
    const filePath = `${path}/${file.name}`

    let extension = file.name.split('.').pop();
    if (extension === "gltf") {
        glbFilename = file.name.substr(file.name.lastIndexOf('/') + 1, file.name.lastIndexOf('.'));
        gltf = JSON.parse(readFile(filePath))
    } else {
        fileBlobs[file.name.toLowerCase()] = readFile(filePath)
    }

    checkRemaining();
}

function checkRemaining() {
    remainingFilesToProcess--;
    if (remainingFilesToProcess === 0) {
        outputBuffers = [];
        bufferOffsetMap = new Map();
        bufferOffset = 0;
        processBuffers().then(fileSave);
    }
}

function processBuffers() {
    if (!gltf) {
        console.error("No gltf found. Use -h for help.");
        process.abort();
    }

    var pendingBuffers = gltf.buffers.map(function (buffer, bufferIndex) {
        let data = fileBlobs[buffer.uri.toString().toLowerCase()]
        if (data !== undefined) {
            outputBuffers.push(data);
        }
        delete buffer.uri;
        buffer.length = data.length;
        bufferOffsetMap.set(bufferIndex, bufferOffset);
        bufferOffset += alignedLength(data.length);
    });

    return Promise.all(pendingBuffers)
        .then(function () {
            var bufferIndex = gltf.buffers.length;
            var images = gltf.images || [];
            var pendingImages = images.map(function (image) {
                let data = fileBlobs[image['uri'].toLowerCase()]

                var bufferView = {
                    buffer: 0,
                    byteOffset: bufferOffset,
                    byteLength: data.length,
                };
                bufferOffsetMap.set(bufferIndex, bufferOffset);
                bufferIndex++;
                bufferOffset += alignedLength(data.length);
                var bufferViewIndex = gltf.bufferViews.length;
                gltf.bufferViews.push(bufferView);
                outputBuffers.push(data);
                image['bufferView'] = bufferViewIndex;
                image['mimeType'] = getMimeType(image.uri);
                delete image['uri'];
            });
            return Promise.all(pendingImages);
        });
}

function fileSave() {
    var Binary = {
        Magic: 0x46546C67
    };

    for (var _i = 0, _a = gltf.bufferViews; _i < _a.length; _i++) {
        var bufferView = _a[_i];
        if (bufferView.byteOffset === undefined) {
            bufferView.byteOffset = 0;
        } else {
            bufferView.byteOffset = bufferView.byteOffset + bufferOffsetMap.get(bufferView.buffer);
        }
        bufferView.buffer = 0;
    }
    var binBufferSize = bufferOffset;
    gltf.buffers = [{
        byteLength: binBufferSize
    }];

    var jsonBuffer = encoder.encode(JSON.stringify(gltf));
    var jsonAlignedLength = alignedLength(jsonBuffer.length);
    var padding;
    if (jsonAlignedLength !== jsonBuffer.length) {

        padding = jsonAlignedLength - jsonBuffer.length;
    }
    var totalSize = 12 + // file header: magic + version + length
        8 + // json chunk header: json length + type
        jsonAlignedLength +
        8 + // bin chunk header: chunk length + type
        binBufferSize;
    var finalBuffer = new ArrayBuffer(totalSize);
    var dataView = new DataView(finalBuffer);
    var bufIndex = 0;
    dataView.setUint32(bufIndex, Binary.Magic, true);
    bufIndex += 4;
    dataView.setUint32(bufIndex, 2, true);
    bufIndex += 4;
    dataView.setUint32(bufIndex, totalSize, true);
    bufIndex += 4;
    // JSON
    dataView.setUint32(bufIndex, jsonAlignedLength, true);
    bufIndex += 4;
    dataView.setUint32(bufIndex, 0x4E4F534A, true);
    bufIndex += 4;

    for (var j = 0; j < jsonBuffer.length; j++) {
        dataView.setUint8(bufIndex, jsonBuffer[j]);
        bufIndex++;
    }
    if (padding !== undefined) {
        for (var j = 0; j < padding; j++) {
            dataView.setUint8(bufIndex, 0x20);
            bufIndex++;
        }
    }

    // BIN
    dataView.setUint32(bufIndex, binBufferSize, true);
    bufIndex += 4;
    dataView.setUint32(bufIndex, 0x004E4942, true);
    bufIndex += 4;
    for (var i = 0; i < outputBuffers.length; i++) {
        var bufoffset = bufIndex + bufferOffsetMap.get(i);
        var buf = new Uint8Array(outputBuffers[i]);
        var thisbufindex = bufoffset;
        for (var j = 0; j < buf.length; j++) {
            dataView.setUint8(thisbufindex, buf[j]);
            thisbufindex++;
        }
    }

    const buffer = new Buffer.from(finalBuffer);
    const outputPathname = `${outputPath}/${glbFilename}.glb`

    fs.writeFileSync(outputPathname, buffer, {encoding: "utf-8"})
}

function getConversionDurationMs() {
    const stopTime = getTimeAsMs();

    return Math.round(stopTime - startTime);
}

function alignedLength(value) {
    var alignValue = 4;
    if (value == 0) {
        return value;
    }
    var multiple = value % alignValue;
    if (multiple === 0) {
        return value;
    }
    return value + (alignValue - multiple);
}

function getMimeType(filename) {
    if (!filename) {
        console.log("not defined", filename);
        return 'application/octet-stream'
    }
    console.log("defined", filename);

    for (var mimeType in gltfMimeTypes) {
        for (var extensionIndex in gltfMimeTypes[mimeType]) {
            var extension = gltfMimeTypes[mimeType][extensionIndex];
            if (filename.toLowerCase().endsWith('.' + extension)) {
                return mimeType;
            }
        }
    }
    return 'application/octet-stream';
}

function getFilesInDir(path) {
    return fs.readdirSync(path, {encoding: "utf8", withFileTypes: true})
        .filter(file => !file.isDirectory())
}

function readFile(path) {
    return fs.readFileSync(path);
}

var gltfMimeTypes = {
    'image/png': ['png'],
    'image/jpeg': ['jpg', 'jpeg'],
    'text/plain': ['glsl', 'vert', 'vs', 'frag', 'fs', 'txt'],
    'image/vnd-ms.dds': ['dds']
};

const inputFiles = getFilesInDir(inputPath);
processFiles(inputPath, inputFiles)