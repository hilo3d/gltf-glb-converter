#gltf-glb-converter

Made to convert a .gltf and its assets to a .glb file.
Made for node 14.9.0

## usage
Show help : `node converter.js -h`

Convert given directory and write .glb in current working directory.`node converter.js input_dir` 

Convert given directory and write .glb in given output directory.`node converter.js input_dir output_dir` 

If input_dir is a file instead of a directory, this converter will use the file's parent directory as input_dir.

Ensure that directory contains only one *.gltf* file.